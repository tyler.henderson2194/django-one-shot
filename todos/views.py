from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_detail = TodoList.objects.get(id=id)
    context = {
        "todo_detail": todo_detail
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_detail = form.save(False)
            todo_detail.save()
            return redirect("todo_list_detail", id=todo_detail.id)
    else:
        form = TodoForm()
        context = {
            "form": form
        }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_update)
        if form.is_valid():
            todo_update = form.save()
            return redirect("todo_list_detail", id=todo_update.id)
    else:
        form = TodoForm(instance=todo_update)
    context = {
        "form": form
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    todo_list = TodoList.objects.all
    # todo_list needs to be defined before if, and needs to be available regardless of content
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            todo_create = form.save()
            todo_create.user = request.user
            todo_create.save()
            return redirect("todo_list_detail", id=todo_create.id)

    else:
        form = ItemForm()
    context = {
        "form": form,
        "todo_list": todo_list,
    }

    return render(request, "todos/task_create.html", context)


def todo_item_update(request, id):
    item_update = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item_update)
        if form.is_valid:
            item_update = form.save()
            item_update.save()
            return redirect("todo_list_update", id=item_update.id)
    else:
        form = TodoItem(instance=item_update)

    context = {
        "form": form
    }

    return render(request, "todos/item_edit.html", context)
